#!/bin/bash

# Nastavte emailovú adresu, na ktorú chcete zasielať výstup
recipients=("dmaget@gmail.com" "patrik.cesticky@gmail.com")

# Funkcia na zaslanie emailu cez Postfix
send_email() {
    subject="$1"
    body="$2"
    for recipient in "${recipients[@]}"; do
        echo -e "To: $recipient\nSubject: $subject\n$body" | /usr/sbin/sendmail -t
    done
}

# Monitorovanie voľného miesta vo file systéme
free_space=$(df -h / | awk 'NR==2 {print $4}')

# Monitorovanie bežiacich procesov
running_processes=$(ps aux | wc -l)

# Monitorovanie prihlásených užívateľov
logged_in_users=$(who)

# Monitorovanie pingovania na zadané webové stránky
ping_result=$(ping -c 3 dusanmaget.sk)
ping_result+="\n\n$(ping -c 3 google.com)"
ping_result+="\n\n$(ping -c 3 youtube.com)"

# Vytvorenie správy pre email
email_subject="Report z Monitorovania"
email_body="Volne Miesto vo Filesysteme: $free_space\n\n"
email_body+="Beziace Procesy: $running_processes\n\n"
email_body+="Prihlaseni Uzivatelia:\n$logged_in_users\n\n"
email_body+="Vysledky Ping:\n$ping_result\n"

# Zaslanie emailu
send_email "$email_subject" "$email_body"

